for i in *.png;
  do name=`echo "$i" | cut -d'.' -f1`
  echo "$name"
  cwebp -q 80 "$i" -o "${name}.webp"
done

for i in *.jpg;
  do name=`echo "$i" | cut -d'.' -f1`
  echo "$name"
  cwebp -q 80 "$i" -o "${name}.webp"
done